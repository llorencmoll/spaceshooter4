﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

	private Material mat;
	public float smooth = 1.0f;
	private float offsetY = 0.0f;

	private Vector2 textoffset;


	// Use this for initialization
	void Awake () {
		mat = GetComponent<Renderer> ().material;
		textoffset = new Vector2 (0, offsetY);
	}
	
	// Update is called once per frame
	void Update () {
		offsetY += Time.deltaTime * smooth;
		if(offsetY >= 100) offsetY -= 100;

		textoffset.y = offsetY;
		mat.SetTextureOffset("_MainTex", textoffset);
	}
}
