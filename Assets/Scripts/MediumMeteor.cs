﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumMeteor : TinyMeteor {

	protected override void Explode(){

		//Lanza dos meteoritos Medium
		MeteorManager.getInstance().LaunchMeteor(1,transform.position, new Vector2(-4,-4),-5);
		MeteorManager.getInstance().LaunchMeteor(1,transform.position, new Vector2(4,-4),-5);

		base.Explode ();


	}
}
