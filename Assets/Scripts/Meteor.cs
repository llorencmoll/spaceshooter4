﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Meteor : MonoBehaviour {

	protected bool launched;
	protected Vector2 moveSpeed;
	protected float rotationSpeed;
	public ParticleSystem particle;

	private Transform graphics;
	private Collider2D meteorCollider;

	public AudioSource audioExplosion;


	private Vector3 iniPos;

	void Start(){
		iniPos = transform.position;
		graphics = transform.GetChild (0);
		meteorCollider = GetComponent<Collider2D> ();
	}

	public void LaunchMeteor(Vector3 position,Vector2 direction, float rotation){
		transform.position = position;
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		moveSpeed = direction;
		rotationSpeed = rotation;
		graphics.gameObject.SetActive (true);
		meteorCollider.enabled = true;
	}

	// Update is called once per frame
	protected void Update () {
		if (launched) {
			transform.Translate (moveSpeed.x*Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			graphics.Rotate(0,0, rotationSpeed);
		}
	}

	protected void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet") {
			Explode ();
		}else if(other.tag == "Finish") {
			Reset ();
		}
	}

	protected void Reset(){
		transform.position = iniPos;
		launched = false;
	}

	protected virtual void Explode(){
		audioExplosion.Play ();
		meteorCollider.enabled = false;
		launched = false;
		particle.Play ();
		graphics.gameObject.SetActive (false);
		Invoke ("Reset", 1);
	}
}
